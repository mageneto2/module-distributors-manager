<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com =>
 */

namespace MacPain\DistributorsManager\Mapper\BrukeoOldDataSource;

class Voivodeship implements \Magento\Framework\Data\OptionSourceInterface
{

    protected array $voivodeships = [
        '02' => 'dolnośląskie',
        '04' => 'kujawsko-pomorskie',
        '06' => 'lubelskie',
        '08' => 'lubuskie',
        '10' => 'łódzkie',
        '12' => 'małopolskie',
        '14' => 'mazowieckie',
        '16' => 'opolskie',
        '18' => 'podkarpackie',
        '20' => 'podlaskie',
        '22' => 'pomorskie',
        '24' => 'śląskie',
        '26' => 'świętokrzyskie',
        '28' => 'warmińsko-mazurskie',
        '30' => 'wielkopolskie',
        '32' => 'zachodniopomorskie',
    ];

    public function toOptionArray(): array
    {
        $result = [];
        foreach ($this->voivodeships as $value => $label) {
            $result[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $result;
    }

}
