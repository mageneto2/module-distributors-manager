<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace MacPain\DistributorsManager\Mapper\BrukeoOldDataSource;

class Manufacturer implements \Magento\Framework\Data\OptionSourceInterface
{

    protected array $manufacturers = [
         129 => 'Betard',
          15 => 'Bruk-Bet',
        1885 => 'Bruk Czyżowice',
          18 => 'Drewbet',
        1808 => 'Drewbet - nawierzchnie',
        1809 => 'Drewbet - ogrodzenia',
         127 => 'Jadar',
          17 => 'Kost-Bet',
          16 => 'Libet',
         131 => 'Polbruk',
         128 => 'Pozbruk',
         130 => 'Semmelrock',
        2008 => 'Ziel-Bruk',
        2042 => 'Drogbruk',
        2048 => 'Kamal',
        2255 => 'ZPB Kaczmarek',
        2334 => 'Joniec',
        2523 => 'Gladio',
        2577 => 'Vestone',
        2606 => 'Buszrem',
        2749 => 'Superbruk',
        2816 => 'Pebek'
    ];

    public function toOptionArray(): array
    {
        $result = [];
        foreach ($this->manufacturers as $value => $label) {
            $result[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $result;
    }

}
