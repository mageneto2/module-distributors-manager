<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace MacPain\DistributorsManager\Mapper\BrukeoOldDataSource;

class Type implements \Magento\Framework\Data\OptionSourceInterface
{

    protected array $types = [
        2 => 'Architekt',
        1 => 'Dystrybutor',
        3 => 'Wykonawca'
    ];

    public function toOptionArray(): array
    {
        $result = [];
        foreach ($this->types as $value => $label) {
            $result[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $result;
    }

}
