<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace MacPain\DistributorsManager\Mapper\BrukeoOldDataSource;

class Offer implements \Magento\Framework\Data\OptionSourceInterface
{

    protected array $offers = [
        2389 => 'projektowanie',
        2387 => 'sprzedaż wyrobów betonowych',
        2388 => 'transport',
        2390 => 'układanie'
    ];

    public function toOptionArray(): array
    {
        $result = [];
        foreach ($this->offers as $value => $label) {
            $result[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $result;
    }

}
