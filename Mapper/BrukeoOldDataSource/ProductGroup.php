<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace MacPain\DistributorsManager\Mapper\BrukeoOldDataSource;

class ProductGroup implements \Magento\Framework\Data\OptionSourceInterface
{

    protected array $productGroups = [
         9 => 'Kostka brukowa',
        10 => 'Mała architektura',
        11 => 'Obrzeża i krawężniki',
        12 => 'Ogrodzenia i murki',
        13 => 'Palisady',
        20 => 'Prefabrykaty betonowe',
        14 => 'Produkty uzupełniające',
        15 => 'Płyty tarasowe i chodnikowe',
        16 => 'Płyty wielkoformatowe',
        17 => 'Ściany i elewacje',
        18 => 'Stopnie schodowe',
    ];

    public function toOptionArray(): array
    {
        $result = [];
        foreach ($this->productGroups as $value => $label) {
            $result[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $result;
    }

}
