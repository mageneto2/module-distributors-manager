<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace MacPain\DistributorsManager\Setup\Patch\Data;

class AssignDistributorAttributesToDistributorAttributeSet implements \Magento\Framework\Setup\Patch\DataPatchInterface
{

    protected array $attributeCodesToAssign = [
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_ABOUT_US_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_CITY_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_EMAIL_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_EXHIBITION_GARDEN_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_EXHIBITION_GARDEN_SIZE_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_LATITUDE_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_LONGITUDE_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_MANUFACTURERS_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_NIP_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_OFFER_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_OPENING_HOURS_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_POST_CODE_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_PRODUCT_GROUPS_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_STREET_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_TELEPHONE_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_TELEPHONE_2_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_TYPES_ATTRIBUTE_CODE,
        \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_VOIVODESHIPS_ATTRIBUTE_CODE,
    ];

    protected \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup;
    protected \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory;
    protected \Magento\Catalog\Model\Config $config;
    protected \Magento\Eav\Api\AttributeManagementInterface $attributeManagement;
    protected \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetcollectionFactory;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Magento\Catalog\Model\Config $config,
        \Magento\Eav\Api\AttributeManagementInterface $attributeManagement,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetcollectionFactory
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->config = $config;
        $this->attributeManagement = $attributeManagement;
        $this->attributeSetcollectionFactory = $attributeSetcollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attributeSetId = $this->getDistributorAttributeSetId();
        $attributeGroupId = $this->config->getAttributeGroupId(
            $attributeSetId,
            \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_ATTRIBUTE_SET_GROUP_NAME
        );

        foreach ($this->attributeCodesToAssign as $attributeCodeToAssign) {
            $this->attributeManagement->assign(
                \Magento\Catalog\Model\Product::ENTITY,
                $attributeSetId,
                $attributeGroupId,
                $attributeCodeToAssign,
                999
            );
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorAboutUsProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorCityProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorEmailProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorExhibitionGardenProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorExhibitionGardenSizeProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorLatitudeProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorLongitudeProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorManufacturersProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorNipProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorOfferProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorOpeningHoursProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorPostCodeProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorProductGroupsProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorStreetProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorTelephoneProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorTelephone2ProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorTypesProductAttribute::class,
            \MacPain\DistributorsManager\Setup\Patch\Data\AddDistributorVoivodeshipsProductAttribute::class,
        ];
    }

    protected function getDistributorAttributeSetId(): int
    {
        /** @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection $attributeSets */
        $attributeSets = $this->attributeSetcollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('attribute_set_name', \MacPain\DistributorsManager\Helper\Constants::DISTRIBUTOR_ATTRIBUTE_SET_NAME);

        return $attributeSets->getFirstItem()->getData('attribute_set_id');
    }

}
