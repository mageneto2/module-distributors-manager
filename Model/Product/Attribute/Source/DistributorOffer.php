<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace MacPain\DistributorsManager\Model\Product\Attribute\Source;

class DistributorOffer extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    public function getAllOptions(): array
    {
        $this->_options = [
            ['value' => 'projektowanie', 'label' => 'projektowanie'],
            ['value' => 'sprzedaz_wyrobow_betonowych', 'label' => 'sprzedaż wyrobów betonowych'],
            ['value' => 'transport', 'label' => 'transport'],
            ['value' => 'ukladanie', 'label' => 'układanie']
        ];

        return $this->_options;
    }

}

