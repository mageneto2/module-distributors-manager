<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace MacPain\DistributorsManager\Model\Product\Attribute\Source;

class DistributorManufacturer extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    public function getAllOptions(): array
    {
        $this->_options = [
            ['value' => 'betard', 'label' => 'Betard'],
            ['value' => 'bruk_bet', 'label' => 'Bruk-Bet'],
            ['value' => 'bruk_czyzowice', 'label' => 'Bruk Czyżowice'],
            ['value' => 'drewbet', 'label' => 'Drewbet'],
            ['value' => 'drewbet_nawierzchnie', 'label' => 'Drewbet - nawierzchnie'],
            ['value' => 'drewbet_ogrodzenia', 'label' => 'Drewbet - ogrodzenia'],
            ['value' => 'jadar', 'label' => 'Jadar'],
            ['value' => 'kost_bet', 'label' => 'Kost-Bet'],
            ['value' => 'libet', 'label' => 'Libet'],
            ['value' => 'polbruk', 'label' => 'Polbruk'],
            ['value' => 'pozbruk', 'label' => 'Pozbruk'],
            ['value' => 'semmelrock', 'label' => 'Semmelrock'],
            ['value' => 'ziel_bruk', 'label' => 'Ziel-Bruk'],
            ['value' => 'drogbruk', 'label' => 'Drogbruk'],
            ['value' => 'kamal', 'label' => 'Kamal'],
            ['value' => 'zpb_kaczmarek', 'label' => 'ZPB Kaczmarek'],
            ['value' => 'joniec', 'label' => 'Joniec'],
            ['value' => 'gladio', 'label' => 'Gladio'],
            ['value' => 'vestone', 'label' => 'Vestone'],
            ['value' => 'buszrem', 'label' => 'Buszrem'],
            ['value' => 'superbruk', 'label' => 'Superbruk'],
            ['value' => 'pebek', 'label' => 'Pebek']
        ];

        return $this->_options;
    }
}

