<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace MacPain\DistributorsManager\Model\Product\Attribute\Source;

class DistributorType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    public function getAllOptions(): array
    {
        $this->_options = [
            ['value' => 'architekt', 'label' => 'Architekt'],
            ['value' => 'dystrybutor', 'label' => 'Dystrybutor'],
            ['value' => 'wykonawca', 'label' => 'Wykonawca']
        ];

        return $this->_options;
    }

}

