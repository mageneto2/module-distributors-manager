<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace MacPain\DistributorsManager\Model\Product\Attribute\Source;

class DistributorVoivodeship extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    public function getAllOptions(): array
    {
        $this->_options = [
            ['value' => 'dolnoslaskie', 'label' => 'dolnośląskie'],
            ['value' => 'kujawsko_pomorskie', 'label' => 'kujawsko-pomorskie'],
            ['value' => 'lubelskie', 'label' => 'lubelskie'],
            ['value' => 'lubuskie', 'label' => 'lubuskie'],
            ['value' => 'lodzkie', 'label' => 'łódzkie'],
            ['value' => 'malopolskie', 'label' => 'małopolskie'],
            ['value' => 'mazowieckie', 'label' => 'mazowieckie'],
            ['value' => 'opolskie', 'label' => 'opolskie'],
            ['value' => 'podkarpackie', 'label' => 'podkarpackie'],
            ['value' => 'podlaskie', 'label' => 'podlaskie'],
            ['value' => 'pomorskie', 'label' => 'pomorskie'],
            ['value' => 'slaskie', 'label' => 'śląskie'],
            ['value' => 'swietokrzyskie', 'label' => 'świętokrzyskie'],
            ['value' => 'warminsko_mazurskie', 'label' => 'warmińsko-mazurskie'],
            ['value' => 'wielkopolskie', 'label' => 'wielkopolskie'],
            ['value' => 'zachodniopomorskie', 'label' => 'zachodniopomorskie']
        ];

        return $this->_options;
    }

}

