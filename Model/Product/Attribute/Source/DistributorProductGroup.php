<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace MacPain\DistributorsManager\Model\Product\Attribute\Source;

class DistributorProductGroup extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    protected array $useableCategories = [
        'Kostka brukowa',
        'Mała architektura',
        'Obrzeża i krawężniki',
        'Ogrodzenia i murki',
        'Palisady',
        'Prefabrykaty betonowe',
        'Produkty uzupełniające',
        'Płyty tarasowe i chodnikowe',
        'Płyty wielkoformatowe',
        'Ściany i elewacje',
        'Stopnie schodowe'
    ];

    protected \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory;

    public function __construct(\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory)
    {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }

    public function getAllOptions(): array
    {
        return $this->_options = $this->getCategories();
    }

    protected function getCategories(): array
    {
        $result = [];
        $categories =  $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->setStore(0);

        /** @var \Magento\Catalog\Model\Category $category */
        foreach ($categories as $category) {
            if (!in_array($category->getName(), $this->useableCategories)) {
                continue;
            }

            $result[] = [
                'value' => $category->getId(),
                'label' => $category->getName()
            ];
        }

        return $result;
    }

}

